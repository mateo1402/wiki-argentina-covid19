# wiki-argentina-Covid-19

It is built in python and selenium to generate two .csv and .pdf files by scraping this webpage [here](https://es.wikipedia.org/wiki/Pandemia_de_enfermedad_por_coronavirus_de_2020_en_Argentina) for Argentina and this other webpage [here](https://www.worldometers.info/coronavirus/) for world statics

## Installation.

1) git clone https://mateo1402@bitbucket.org/mateo1402/wiki-argentina-covid19.git

2) Go into de Directory created.

3) virtualenv env --python=python3

3) source env/bin/activate

4) pip install -r reqs.txt

5) edit inside statics_argentina.ini and write down in "chromedriver_path = " your path to file chromedriver in your directory and then save it.

### To generate Argentina Statics csv and pdf.

6) python statics_argentina.py

### To generate World Statics csv and pdf.

7) python statics_world.py

### Additional installation

Ubuntu

```
sudo apt-get install chromium-chromedriver
```

Debian

```
sudo apt-get install -y chromium-driver
```

## Note

If the chromedriver version is not compatible, please download from [here](https://chromedriver.chromium.org/downloads) a version compatible with your google-chrome.

## Background

Wikipedia did some effort to translate that data [here](https://es.wikipedia.org/wiki/Pandemia_de_enfermedad_por_coronavirus_de_2020_en_Argentina). That's the only source which can be parsed, and that's the source of this work.

## Output

wiki_argentina.csv

wiki_argentina.pdf

world_statics.csv

world_statics.pdf

## Authors

* **https://linkedin.com/in/ariel-belossi** - *Initial Work*
