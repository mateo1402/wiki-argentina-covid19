import pandas as pd

import configparser, sys

from time import sleep

from selenium import webdriver

from selenium.webdriver.chrome.options import Options


config = configparser.ConfigParser()
archivo_config=sys.argv[0].replace('.py','.ini')
config.read(archivo_config)

class statics:

    global config

    def __init__(self):
        self.statics = [] 

    def add_statics(self, data):
        self.statics.append(data) 

    def get_statics(self):
        print ("Por favor, espere mientras se procesan los dos archivos csv y pdf ....")
    
        options = Options()
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-gpu")
        options.add_argument("--headless")
        
        # Disabling images downloading for time improvements
        options.add_argument('--blink-settings=imagesEnabled=false')
        
        
        driver = webdriver.Chrome(config['Global']['chromedriver_path'], options=options)
        driver.set_window_size(1920, 1080)
        url='https://es.wikipedia.org/wiki/Pandemia_de_enfermedad_por_coronavirus_de_2020_en_Argentina'
        driver.get(url)
    
        sleep(1)
        
        #Header info
        titulos = ['Provincia', 'Casos\nConfirmados', 'Población', 'Total\nMuertes', 'Tot Casos\n/1M pop', 'Recuperados']
    
        #inicializo las estadísticas
        self.add_statics(titulos)
    
        #Body_info
        brands = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[4]/div/table[3]/tbody')
        options = brands.find_elements_by_tag_name('tr')
        for m in options:
            texto = []
            info = m.find_elements_by_tag_name('td')
            i=0
            for t in info:
                if i==0:
                    texto.append(t.text.strip().replace(',','\n'))
                else:
                    texto.append(t.text)
                i = i + 1 
            self.add_statics(texto)
    
        #Bottom info
        brands = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[4]/div/table[3]/tfoot')
        options = brands.find_elements_by_tag_name('tr')
        for m in options:
            texto = []
            info = m.find_elements_by_tag_name('th')
            i=0
            for t in info:
                if i==0:
                    texto.append(t.text.strip().replace(' ',''))
                else:
                    texto.append(t.text)
                i = i + 1
            self.add_statics(texto)
    
        pd.DataFrame(self.statics).to_csv("wiki_argetina.csv",header=None, index=None)


        return(self.statics) 
    
    
    def convertir_pdf(self,data):
    
        fileName = 'wiki_argentina.pdf'
        from reportlab.platypus import SimpleDocTemplate
        
        from reportlab.lib.pagesizes import A4
        
        
        
        pdf = SimpleDocTemplate(
        
            fileName,
        
            pagesize=A4
        
        )
        
        
        
        from reportlab.platypus import Table
        
        table = Table(data)
        
        
        
        # add style
        
        from reportlab.platypus import TableStyle
        
        from reportlab.lib import colors
        
        
        
        style = TableStyle([
        
            ('BACKGROUND', (0,0), (10,0), colors.cyan),
            ('BACKGROUND', (0,25), (-1,25), colors.dimgrey),
        
            ('TEXTCOLOR',(0,0),(-1,0),colors.black),
    
            ('TEXTCOLOR',(0,25),(-1,25),colors.whitesmoke),
            ('TEXTCOLOR',(0,1),(0,-2),colors.blue),
        
        
        
            ('ALIGN',(0,0),(-1,-1),'CENTER'),
        
        
        
            ('FONTNAME', (0,0), (-1,0), 'Courier-Bold'),
            ('FONTNAME', (0,2), (0,-1), 'Courier-Bold'),
        
            ('FONTSIZE', (0,0), (-1,0), 8.5),
            ('FONTSIZE', (0,1), (-1,-1), 8.5),
            ('FONTSIZE', (0,2), (0,-1), 8.5),
        
        
        
            ('BOTTOMPADDING', (0,0), (-1,0), 8.5),
        
        
        
        ])
        
        table.setStyle(style)
        
        
        
        # 2) Alternate backgroud color
        
        rowNumb = len(data)
        
        for i in range(1, rowNumb-1):
    
            if data[i][1] != '0' :
        
                bc_2 = colors.yellow
                tc_2 = colors.black
        
            else:
        
                bc_2 = colors.white
                tc_2 = colors.black
    
    
            if data[i][3] != '0' :
    
                bc_4 = colors.red
                tc_4 = colors.whitesmoke
    
            else:
    
                bc_4 = colors.white
                tc_4 = colors.black
     
                  
            ts = TableStyle(
        
                [('BACKGROUND', (1,i),(1,i), bc_2),('TEXTCOLOR', (1,i), (1,i), tc_2),
                 ('BACKGROUND', (3,i), (3,i), bc_4),('TEXTCOLOR', (3,i), (3,i),tc_4)]
        
            )
        
            table.setStyle(ts)
        
        
        # 3) Add borders
        
        ts = TableStyle(
        
            [
        
            ('BOX',(0,0),(-1,0),1,colors.black),
        
        
            ('GRID',(0,1),(-1,-1),1,colors.darkgrey),
        
            ]
        
        )
        
        table.setStyle(ts)
        
        
        
        elems = []
        
        elems.append(table)
        
        
        
        pdf.build(elems)
        
a = statics()
data = a.get_statics()
a.convertir_pdf(data)
