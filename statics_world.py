import configparser, sys

import pandas as pd 

from time import sleep

from selenium import webdriver

from selenium.webdriver.chrome.options import Options

config = configparser.ConfigParser()
archivo_config=sys.argv[0].replace('.py','.ini')
config.read(archivo_config)


class world_statics():

    global config

    def __init__(self):
        self.statics = []

    def add_statics(self, data):
        self.statics.append(data)

    def get_statics(self):

        global config
    
        options = Options()
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-gpu")
        options.add_argument("--headless")
        
        # Disabling images downloading for time improvements
        options.add_argument('--blink-settings=imagesEnabled=false')
        
        
        driver = webdriver.Chrome(config['Global']['chromedriver_path'], options=options)
        driver.set_window_size(1920, 1080)
        url='https://www.worldometers.info/coronavirus/'
        driver.get(url)
    
        sleep(1)
    
        #Header info
        titulos = ['País\nOtro', 'Total\nCasos', 'Nuevos\nCasos', 'Total\nMuertes', 'Nuevas\nMuertes', 'Total\nSanados', 
                   'Activos\nCasos', 'Serios\nCríticos', 'Tot Casos\n/1M pop', 'Muertes\n/1M pop','Total\nTests','Tests\n/1M pop']
   
        #Body and Bottom info 
        self.add_statics(titulos)
        brands = driver.find_element_by_tag_name('tbody')
        options = brands.find_elements_by_tag_name('tr')
        for m in options:
            texto = []
            info = m.find_elements_by_tag_name('td')
            i=0
            for t in info:
                if i==0:
                    texto.append(t.text.strip().replace(' ','\n'))
                else:
                    texto.append(t.text)
                i = i + 1 
            self.add_statics(texto)
    
        pd.DataFrame(self.statics).to_csv("world_statics.csv",header=None, index=None)
    
        return (self.statics)
    
    
    def convertir_pdf(self,data):
    
        fileName = 'world_statics.pdf'
        from reportlab.platypus import SimpleDocTemplate
        
        from reportlab.lib.pagesizes import A4
        
        
        
        pdf = SimpleDocTemplate(
        
            fileName,
        
            pagesize=A4
        
        )
        
        
        
        from reportlab.platypus import Table
        
        table = Table(data)
        
        
        
        # add style
        
        from reportlab.platypus import TableStyle
        
        from reportlab.lib import colors
        
        
        
        style = TableStyle([
        
            ('BACKGROUND', (0,0), (11,0), colors.cyan),
            ('BACKGROUND', (0,1), (-1,1), colors.dimgrey),
        
            ('TEXTCOLOR',(0,0),(-1,0),colors.black),
    
            ('TEXTCOLOR',(0,1),(-1,1),colors.whitesmoke),
            ('TEXTCOLOR',(0,2),(0,-1),colors.blue),
        
        
        
            ('ALIGN',(0,0),(-1,-1),'CENTER'),
        
        
        
            ('FONTNAME', (0,0), (-1,0), 'Courier-Bold'),
            ('FONTNAME', (0,2), (0,-1), 'Courier-Bold'),
        
            ('FONTSIZE', (0,0), (-1,0), 7.5),
            ('FONTSIZE', (0,1), (-1,-1), 7.5),
            ('FONTSIZE', (0,2), (0,-1), 7.5),
        
        
        
            ('BOTTOMPADDING', (0,0), (-1,0), 7.5),
        
        
        
        ])
        
        table.setStyle(style)
        
        
        
        # 2) Alternate backgroud color
        
        rowNumb = len(data)
        
        for i in range(2, rowNumb):
    
            if data[i][2] != '' :
        
                bc_2 = colors.yellow
                tc_2 = colors.black
        
            else:
        
                bc_2 = colors.white
                tc_2 = colors.white
    
    
            if data[i][4] != '' :
    
                bc_4 = colors.red
                tc_4 = colors.whitesmoke
    
            else:
    
                bc_4 = colors.white
                tc_4 = colors.white
     
                  
            ts = TableStyle(
        
                [('BACKGROUND', (2,i),(2,i), bc_2),('TEXTCOLOR', (2,i), (2,i), tc_2),
                 ('BACKGROUND', (4,i), (4,i), bc_4),('TEXTCOLOR', (4,i), (4,i),tc_4)]
        
            )
        
            table.setStyle(ts)
        
        
        # 3) Add borders
        
        ts = TableStyle(
        
            [
        
            ('BOX',(0,0),(-1,0),1,colors.black),
        
        
            ('GRID',(0,1),(-1,-1),1,colors.darkgrey),
        
            ]
        
        )
        
        table.setStyle(ts)
        
        
        
        elems = []
        
        elems.append(table)
        
        
        
        pdf.build(elems)
       

a = world_statics()
data = a.get_statics()
a.convertir_pdf(data)
 
